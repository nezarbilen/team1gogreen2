##Hello ZiyoTek Family, Welcome to Team1 Gogreen insuarance company
# Create Vpc
resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "main"
  }
}
# Internet gateway
resource "aws_internet_gateway" "team1-I-G-W" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "team1-i-g-w"
  }
}

# Eip and Nat Gateway
resource "aws_eip" "team1-eip" {
  vpc                       = true
  associate_with_private_ip = "10.0.0.5"
}
resource "aws_nat_gateway" "team1-nat" {
  allocation_id = aws_eip.team1-eip.id
  subnet_id     = aws_subnet.publicsn1.id
  depends_on    = [aws_eip.team1-eip]

  tags = {
    Name = "team1-NAT"
  }
}

# Create Subnets
resource "aws_subnet" "publicsn1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "public-sn-1"
  }
}

resource "aws_subnet" "publicsn2" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "public-sn-2"
  }
}

resource "aws_subnet" "privatesn1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.3.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "private-sn-1"
  }
}

resource "aws_subnet" "privatesn2" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.4.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "private-sn-2"
  }
}

resource "aws_subnet" "privatesn3" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.5.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "private-sn-3"
  }
}


resource "aws_subnet" "privatesn4" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.6.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "private-sn-4"
  }
}

# resource "aws_subnet" "privatesn5" {
#   vpc_id     = aws_vpc.main.id
#   cidr_block = "10.0.7.0/24"
#   availability_zone = "us-east-1a"

#   tags = {
#     Name = "private-sn-5"
#   }
# }


# resource "aws_subnet" "privatesn6" {
#   vpc_id     = aws_vpc.main.id
#   cidr_block = "10.0.8.0/24"
#   availability_zone = "us-east-1b"

#   tags = {
#     Name = "private-sn-6"
#   }
# }

# this is public route table
resource "aws_route_table" "public-r-t" {
  vpc_id = aws_vpc.main.id

   route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.team1-I-G-W.id
  }

 # route {
 #   ipv6_cidr_block        = "::/0"
 #   egress_only_gateway_id = aws_egress_only_internet_gateway.foo.id
 # }

  tags = {
    Name = "team1-public-route-table"
  }
}


#This is private Route table

resource "aws_default_route_table" "private-r-t" {
  default_route_table_id = aws_vpc.main.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.team1-nat.id
  }
  tags = {
    Name = "team1-private-route-table"
  }
}

#Subnet associations to the route table
resource "aws_route_table_association" "team1_public1_assoc" {
  subnet_id      = aws_subnet.publicsn1.id
  route_table_id = aws_route_table.public-r-t.id
}

resource "aws_route_table_association" "team1_public2_assoc" {
  subnet_id      = aws_subnet.publicsn2.id
  route_table_id = aws_route_table.public-r-t.id
}

resource "aws_route_table_association" "team1_private1_assoc" {
  subnet_id      = aws_subnet.privatesn1.id
  route_table_id = aws_default_route_table.private-r-t.id
}

resource "aws_route_table_association" "team1_private2_assoc" {
  subnet_id      = aws_subnet.privatesn2.id
  route_table_id = aws_default_route_table.private-r-t.id
}
resource "aws_route_table_association" "team1_private3_assoc" {
  subnet_id      = aws_subnet.privatesn3.id
  route_table_id = aws_default_route_table.private-r-t.id
}

resource "aws_route_table_association" "team1_private4_assoc" {
  subnet_id      = aws_subnet.privatesn4.id
  route_table_id = aws_default_route_table.private-r-t.id
}

# resource "aws_route_table_association" "team1_private5_assoc" {
#   subnet_id      = aws_subnet.privatesn5.id
#   route_table_id = aws_default_route_table.private-r-t.id
# }

# resource "aws_route_table_association" "team1_private6_assoc" {
#   subnet_id      = aws_subnet.privatesn6.id
#   route_table_id = aws_default_route_table.private-r-t.id
# }
