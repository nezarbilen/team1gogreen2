
#---RDS Instances---

resource "aws_db_instance" "TeamOnePrimaryDB" {
  allocated_storage       = 10
  storage_type            = "gp2"
  engine                  = "mysql"
  engine_version          = "5.7"
  instance_class          = "db.t3.micro"
  name                    = "TeamOnePrimaryDB"
  username                = "admin"
  password                = "password1234567891"
  db_subnet_group_name    = aws_db_subnet_group.team1-subnet-group.name
  vpc_security_group_ids  = [aws_security_group.team1_rds_sg.id]
  skip_final_snapshot     = true
  availability_zone       = "us-east-1a"
  apply_immediately       = false
  backup_retention_period = 15
  deletion_protection     = false
  publicly_accessible     = false
  #enabled_cloudwatch_logs_exports = general
  iam_database_authentication_enabled = true
  #replicate_source_db     =   ##KMS_key_id must be specified
}


#RDS Security Group
resource "aws_security_group" "team1_rds_sg" {
  name        = "team1_rds_sg"
  description = "Used for RDS instances"
  vpc_id      = aws_vpc.main.id
  #SQL access from public and private SGs
  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.team1-app-sg.id, aws_security_group.team1-web-sg.id]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

##Create Subnet Group for Data base
resource "aws_db_subnet_group" "team1-subnet-group" {
  name       = "team1-subnet-group"
  subnet_ids = [aws_subnet.privatesn1.id, aws_subnet.privatesn2.id, aws_subnet.privatesn3.id, aws_subnet.privatesn4.id]

  tags = {
    Name = "Team1 DB subnet group"
  }
}

#  resource aws_db_instance "default" {
#    allocated_storage    = 10
#    engine               = "mysql"
#    engine_version       = 5.7
#    instance_class       = "db.t3.micro"
#    name                 = "mydb"
#    username             = "foo"
#    password             = "foobarbaz"
#    parameter_group_name = aws_db_parameter_group.default.name
#    skip_final_snapshot  = true
#    db_subnet_group_name = aws_db_subnet_group.tream1-db-group.name
#    multi_az             = true
#    security_group_names =
#  }

#  resource "aws_db_parameter_group" "default" {
#   name   = "rds-pg"
#   family = "mysql5.7"

#   parameter {
#     name  = "character_set_server"
#     value = "utf8"
#   }

# }

# resource "aws_db_subnet_group" "tream1-db-group" {
#   name       = "main"
#   subnet_ids = [aws_subnet.privatesn3.id, aws_subnet.privatesn4.id]

#   tags = {
#     Name = "Team1 DB subnet group"
#   }
# }

# module "cloudwatch-logs-exporter" {
#   source           = "gadgetry-io/cloudwatch-logs-exporter/aws"
#   version          = "0.0.4"

#   name             = "vpc-flow-logs"
#   log_group        = "/prd/vpc_flow_logs"
#   s3_bucket        = "my-s3-bucket"
#   s3_prefix        = "cloudwatch/prd/vpc_flow_logs"
# }


# resource "aws_db_instance" "team1_replica_db" {

#   instance_class         = var.db_instance_class
#   vpc_security_group_ids = [aws_security_group.team1_rds_sg.id]
#   availability_zone      = data.aws_availability_zones.available.names[1]

#   replicate_source_db = aws_db_instance.group3_primary_db.id
# }
