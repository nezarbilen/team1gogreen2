#APP_TIER


# Launcher Template for App
 resource "aws_launch_configuration" "team1-app-launch-config" {
   image_id             = "ami-047a51fa27710816e"
   instance_type        = "t2.micro"
   security_groups      = [aws_security_group.team1-app-sg.id]
   iam_instance_profile = aws_iam_instance_profile.team1-ec2-profile.name
  # user_data = file("files/userdata.sh")

   lifecycle {
     create_before_destroy = true
   }
} 

# Auto Scaling Group for App tier
 resource "aws_autoscaling_group" "team1_app_asg" {
   launch_configuration = aws_launch_configuration.team1-app-launch-config.id
   vpc_zone_identifier  = [aws_subnet.privatesn1.id, aws_subnet.privatesn2.id]
   min_size             = 2
   max_size             = 6
   desired_capacity     = 4
   target_group_arns = [aws_lb_target_group.app_alb_target_group.arn]
   health_check_type = "ELB"
   tag {
     key                 = "Name"
     value               = "team1-app-asg"
      propagate_at_launch = true
    }

    lifecycle {
     create_before_destroy = true
     }
  }

## web application load balancer for public access
  resource "aws_lb" "team1_app_elb" {
    name               = "team1-app-elb"
    load_balancer_type = "application"
    internal           = true
    subnets = [aws_subnet.privatesn1.id,
    aws_subnet.privatesn2.id]
    security_groups = [aws_security_group.team1-app-sg.id]
    idle_timeout    = 30 #var.elb_timeout
    tags = {
      name = "team1_app_elb"
    }
  }

##Application Load balancer listeners will be created
  resource "aws_lb_listener" "app_alb_listener" {
     load_balancer_arn = aws_lb.team1_app_elb.arn
     port              = 80
     protocol          = "HTTP"

     default_action {
       target_group_arn = aws_lb_target_group.app_alb_target_group.arn
       type             = "forward"
     }
   }

 ### Target Group for ALB 
  resource "aws_lb_target_group" "app_alb_target_group" {
    name     = "app-alb-target-group"
    port     = "80"
    protocol = "HTTP"
    vpc_id   = aws_vpc.main.id
    tags = {
      name = "alb_target_group"
    }
    health_check {
      healthy_threshold   = 5 #var.elb_healthy_threshold
      unhealthy_threshold = 5 #var.elb_unhealthy_threshold
      timeout             = 30 #var.elb_timeout
      interval            = 60 #var.elb_interval
    }
  }

  resource "aws_autoscaling_attachment" "team1_app_asg" {
    alb_target_group_arn   = aws_lb_target_group.app_alb_target_group.arn
    autoscaling_group_name = aws_autoscaling_group.team1_app_asg.id
  }
