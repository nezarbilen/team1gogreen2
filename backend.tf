terraform {
  backend "s3" {
    bucket = "nezar-s3-work"
    key    = "tstate/gogreen.tfstate"
    region = "us-east-1"
  }
}