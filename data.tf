data "aws_ami" "linux-ami-id" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
  ##need to be configured
  #owners = ["your-account-id"]  # Canonical
}